function fechaSolicitud() {
    var f = new Date();
    var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();
    setTimeout('fechaSolicitud()', 1000);
    $('#inputFechaE').get(0).innerHTML = fecha;
  }

$(document).ready(function () {
    var table = $('#tableInforme').DataTable();

    $('#codigoInput').on('keyup', function() {
        table
          .columns(0)
          .search(this.value)
          .draw();
      });
    
      $('#descripcionInput').on('keyup', function() {
        table
          .columns(1)
          .search(this.value)
          .draw();
      });

      $('#estadoInput').on('keyup', function() {
        table
          .columns(2)
          .search(this.value)
          .draw();
      });

      $('#prioridadInput').on('keyup', function() {
        table
          .columns(3)
          .search(this.value)
          .draw();
      });
    
      $('#solicitanteInput').on('keyup', function() {
        table
          .columns(4)
          .search(this.value)
          .draw();
      });
    


    $("#tableInforme").on('click', '.btneditar', function (event) {

        $('#inputFechaE').html(fechaSolicitud());
  });

  
  $('#btnacept').click(function () {

    
    $('#registrarInforme').modal('hide');

  });


});