function fechaSolicitud() {
  var f = new Date();
  var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();
  setTimeout('fechaSolicitud()', 1000);
  $('#inputFechaE').get(0).innerHTML = fecha;
}

$(document).ready(function () {

  var table = $('#tableMantenimiento').DataTable();
  $('#btnSuccess').click(function () {

    var categoria = $('#selectTipoCateMant option:selected').html();
    var f = new Date();
    var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();
    var descripcion = document.getElementById('inputdescripcionMan').value;


    table.row.add(["1", categoria, "Asignado", fecha, "<button style='' type='button' class='btn btn-danger btn-sm btnborrarA'><i class='fa fa-times'></i></button>", descripcion
    ]).draw()

    document.getElementById("inputUbicacionMant").value = '';
    document.getElementById('inputdescripcionMan').value = '';

    $('#selectTipoCateMant option:first').prop('selected', true);
    document.getElementById("ubicacionM").style.display = 'none';
    document.getElementById("descripcionMant").style.display = 'none';
    document.getElementById("Subcategoria").style.display = 'none';
    document.getElementById("btnSuccess").style.display = 'none';

  });

  $("#tableMantenimiento").on("click", ".btnborrarA", function () {
    table.row($(this).parents('tr')).remove().draw(false);
  });

});

function subcategorias() {
  var opt = $('#selectTipoCateMant').get(0).value;
  $('#ubicacionM').get(0).style.display = '';
  console.log(opt)

  document.getElementById('ubicacionM').style.display = 'none';
  document.getElementById('Subcategoria').style.display = 'none';
  document.getElementById('btnSuccess').style.display = 'none';
  document.getElementById('descripcionMant').style.display = 'none';

  switch (opt) {

    case '1':

      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Reparación aire acondicionado</option><option value="2">Limpieza aire acondicionado</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';

      break;
    case '2':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Limpieza áreas comunes</option><option value="2">Limpieza área especifica</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';

      break;
    case '3':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Reparación del Ascensor</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';

      break;
    case '4':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Reparación/ Reporte Lavamanos</option><option value="2">Mantenimiento lavamanos</option><option value="3">Reparación Sanitario/orinales</option><option value="4">Mantenimiento Sanitario/orinales</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';;
      break;
    case '5':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Reparación Puertas</option><option value="2">Instalación de Puertas</option><option value="3">Reparación Sillas</option><option value="4">Reparación de Ventanas</option><option value="5">Cambio de vidrio</option><option value="6">Duplicado de llaves</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';

      break;
    case '6':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Reparación Chapa de puerta</option><option value="2">Cambio de Chapa de Puerta</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';

      break;
    case '7':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Limpieza de luminarias</option><option value="2">Instalación de luminarias</option><option value="3">Cambio de luminaria</option><option value="4">Reporte daño eléctrico</option><option value="5">Instalación de Tomas corriente</option><option value="6">Instalación de Tomas corriente</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';
      break;
    case '8':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Reparación línea telefónica</option><option value="2">Instalación línea telefónica</option><option value="3">Traslado línea telefónica</option><option value="4">Retiro línea telefónica</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';
      break;
    case '9':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Reporte gotera</option><option value="2">Reporte daños tablilla, vigas</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';
      break;
    case '10':
      var select = '<label for="inputCategoriaMant" style="font-weight:bold">Subcategoria</label><i class="fa fa-question-circle" aria-hidden="true"title="Debe seleccionar la categoria de la solicitud"></i><select class="form-control"><option value="0">Seleccione</option><option value="1">Recolección de residuos peligrosos</option><option value="2">Recolección de reciclaje</option><option value="3">Intervención fumigación</option><option value="4">Reporte daños tubería</option></select>';
      document.getElementById('Subcategoria').style.display = '';
      document.getElementById('Subcategoria').innerHTML = select;
      document.getElementById('ubicacionM').style.display = '';
      document.getElementById('descripcionMant').style.display = '';
      document.getElementById('btnSuccess').style.display = '';
      break;

  }



}


