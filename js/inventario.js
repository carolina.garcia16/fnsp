function fechaSolicitud() {
  var f = new Date();
  var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();
  return fecha;
}

$(document).ready(function () {
  var table = $('#tableInventario').DataTable();

  $('#codigo').on('keyup', function () {
    table
      .columns(0)
      .search(this.value)
      .draw();
  });

  $('#dispositivo').on('keyup', function () {
    table
      .columns(1)
      .search(this.value)
      .draw();
  });
  $('#marca').on('keyup', function () {
    table
      .columns(2)
      .search(this.value)
      .draw();
  });

  $('#modelo').on('keyup', function () {
    table
      .columns(3)
      .search(this.value)
      .draw();
  });



  $('#tabInventario a:first').on('click', function () {
    table
      .columns(1)
      .search("")
      .draw();
  });

  $('#tabInventario a:eq( 1 )').on('click', function () {
    table
      .columns(1)
      .search("Impresora")
      .draw();
  });

  $('#tabInventario a:eq( 2 )').on('click', function () {
    table
      .columns(1)
      .search("Computador")
      .draw();
  });
  $('#tabInventario a:eq( 3 )').on('click', function () {
    table
      .columns(1)
      .search("Portatil")
      .draw();
  });

  $('#tabInventario a:eq( 4 )').on('click', function () {
    table
      .columns(1)
      .search("Televisor")
      .draw();
  });
  $('#tabInventario a:eq( 5 )').on('click', function () {
    table
      .columns(1)
      .search("Red")
      .draw();
  });
  $('#tabInventario a:eq( 6 )').on('click', function () {
    table
      .columns(1)
      .search("Video Bean")
      .draw();
  });
  $('#tabInventario a:eq( 7 )').on('click', function () {
    table
      .columns(1)
      .search("Servidor")
      .draw();
  });

  $('#tabInventario a:last').on('click', function () {
    table
      .columns(1)
      .search("Otro")
      .draw();
  });


  $(document).on("click", ".btnRegistro", function () {
    $('#inputFechaE').html(fechaSolicitud());
  });

  $('#btnacept').click(function () {

    var dispositivo = $('#selectEquipo option:selected').html();
    var f = new Date();
    var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
    var marca =$('#selectMarca option:selected').html();
    var modelo = document.getElementById('inputModelo').value;
    var tipoUso =  $('#selectTipoUso option:selected').html();
    var nombre = document.getElementById("nombre").innerHTML;
    var observacion=document.getElementById("inputObservacion").value;

    if (dispositivo == "Dispositivos de Red") {
      dispositivo = "Red";
    }

    table.row.add(["1", dispositivo, marca, modelo, tipoUso, nombre, fecha, observacion ,"<a  data-toggle='modal' href='#editarEquipo' style='margin-right: 5px; background:rgb(40, 167, 69);' class='btnedit btneditar'><i class='fa fa-list' style='color:white;'></i></a><button style='margin-right: 5px;' type='button' class='btnelimin btnborrarA'><i class='fa fa-times'></i></button>"
    ]).draw();
    $('#registrarEquipo').modal('hide');

  });

  $(document).on("hidden.bs.modal", "#registrarEquipo", function () {
    $(this).find("#inputMarca").val("");
    $(this).find("#inputModelo").val("");
    $(this).find("#inputEstado").val("");
    $(this).find('#selectEquipo option:first').prop('selected', true);
  });

  $("#tableInventario").on("click", ".btnborrarA", function () {
    table.row($(this).parents('tr')).remove().draw(false);
  });

  $("#tableInventario").on('click', '.btneditar', function (event) {

    
    var dispositivoEdit = $(this).closest('tr').find('td:nth-child(2)').text();
    var nombreEdit = document.getElementById("nombre").innerHTML;//Traer el nombre de quien está activo en el sistema y modifica

    $("#selectEquipoEdit").val(dispositivoEdit);

    $('#inputFechaEdit').html(fechaSolicitud());
    $('#selectMarcaEdit').val($(this).closest('tr').find('td:nth-child(3)').text());
    $('#inputModeloEdit').val($(this).closest('tr').find('td:nth-child(4)').text());
    $('#inputTipoUsoEdit').val($(this).closest('tr').find('td:nth-child(5)').text());
    $('#inputObservacionEdit').val($(this).closest('tr').find('td:nth-child(8)').text());


  });

  $('#btnEditarA').click(function () {

    var row = table.row(this).index();
    var f = new Date();
    var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear();
    var dispositivoEditado = $('#selectEquipoEdit option:selected').html();
    var marcaEdit = $('#selectMarcaEdit').val();
    var modeloEdit = $('#inputModeloEdit').val();
    var estadoEdit = $('#selectTipoUsoEdit').val();
    var registroNuevo = $('#nombreEdit').innerHTML;
    var observacionEdit= $('#inputObservacionEdit').val();

    table.row(this).invalidate().draw();

    table.cell(row, 1).data(dispositivoEditado);
    table.cell(row, 2).data(marcaEdit);
    table.cell(row, 3).data(modeloEdit);
    table.cell(row, 4).data(estadoEdit);
    table.cell(row, 5).data(registroNuevo);
    table.cell(row, 6).data(fecha);
    table.cell(row, 7).data(observacionEdit);

    $('#editarEquipo').modal('hide');

  });

  $(document).on("hidden.bs.modal", "#editarEquipo", function () {
    $(this).find('#selectMarcaEdit option:first').prop('selected', true);
    $(this).find("#inputModeloEdit").val("");
    $(this).find("#inputEstadoEdit").val("");
    $(this).find('#selectEquipoEdit option:first').prop('selected', true);
  });

});

function tipoDispositivo() {
  var opt = document.getElementById('selectEquipo').value;

  document.getElementById('computadores').style.display = "none";

  if (opt == "Computador") {
    document.getElementById('computadores').style.display = "";
  }
}

function marca() {
  $("#selectMarca").empty()
  var dispositivo = document.getElementById('selectEquipo').value;
  var marca = document.getElementById('selectMarca');

  var opciones;

  switch (dispositivo) {

    case "Impresora":
      
      opciones = ["Seleccione", "Kyosera", "Kyosera ECOSYS", "Kyosera TASKalfa", "HP OfficeJet", "HP Color LaserJet"]
     
      break;

    case "Portatil":
      
      opciones = ["Seleccione", "Lenovo", "Lenovo IdeaPad", "Kyosera TASKalfa", "HP OfficeJet", "HP Color LaserJet"]
      tipoDispositivo();
      break;

    case "Computador":
    
      opciones = ["Seleccione", "Acer Aspire", "Lenovo", "Lenovo ThinkCentre", "Lenovo ThinkPad", "Dell Optiplex", "HP Compaq", "HP AIO"]
      break;

    case "Televisor":
      opciones=["Seleccione"]
      break;

    case "Dispositivos de Red":
      break;
    case "Video Bean":
      opciones=["Seleccione"]
      break;
    case "Servidor":
      opciones=["Seleccione"]
      break;
    case "Otro":
      break;
  
  }
  

  for (var i = 0; i < opciones.length; i++) {
    var opcion = document.createElement("option");
    opcion.value = opciones[i];
    opcion.text = opciones[i];
    marca.appendChild(opcion);
  }


}

function marcaEdit() {
  $("#selectMarcaEdit").empty()
  var dispositivo = document.getElementById('selectEquipoEdit').value;
  //console.log()
  var marca = document.getElementById('selectMarcaEdit');

  var opciones;

  switch (dispositivo) {

    case "Impresora":
      
      opciones = ["Seleccione", "Kyosera", "Kyosera ECOSYS", "Kyosera TASKalfa", "HP OfficeJet", "HP Color LaserJet"]
     
      break;

    case "Portatil":
      
      opciones = ["Seleccione", "Lenovo", "Lenovo IdeaPad", "Kyosera TASKalfa", "HP OfficeJet", "HP Color LaserJet"]
      tipoDispositivo();
      break;

    case "Computador":
    
      opciones = ["Seleccione", "Acer Aspire", "Lenovo", "Lenovo ThinkCentre", "Lenovo ThinkPad", "Dell Optiplex", "HP Compaq", "HP AIO"]
      break;

    case "Televisor":
      opciones=["Seleccione"]
      break;

    case "Dispositivos de Red":
      break;
    case "Video Bean":
      opciones=["Seleccione"]
      break;
    case "Servidor":
      opciones=["Seleccione"]
      break;
    case "Otro":
      break;
  
  }
  

  for (var i = 0; i < opciones.length; i++) {
    var opcion = document.createElement("option");
    opcion.value = opciones[i];
    opcion.text = opciones[i];
    marca.appendChild(opcion);
  }


}