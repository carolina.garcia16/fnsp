function fechaSolicitud() {
  var f = new Date();
  var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();
  setTimeout('fechaSolicitud()', 1000);
  $('#inputFecha').get(0).innerHTML = fecha;
}

$(document).ready(function () {
  var table = $('#tableSoporte').DataTable();

  $('#subButton').click(function () {

    var categoria = $('#selectTipoCateSopor option:selected').html();
    var f = new Date();
    var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();
    var descripcion = document.getElementById('inputdescripcion').value;

    table.row.add(["1", categoria, "Abierta", fecha, "<button style='text-align: center;' type='button' class='btnelimin btnborrarA'><i class='fa fa-times'></i></button>", descripcion
    ]).draw();

    
    document.getElementById("inputOtro").value= '';
    document.getElementById('inputdescripcion').value = '';
    $('#selectTipoCateSopor option:first').prop('selected', true);
    $('#inputDispositivos option:first').prop('selected', true);
    
    document.getElementById("inputOtro").style.display = 'none';
    document.getElementById("inputDisp").style.display = 'none';
    document.getElementById("soporteEquipo").style.display = 'none';

  });

  $("#tableSoporte").on("click", ".btnborrarA", function () {
    table.row($(this).parents('tr')).remove().draw(false);
  });

});

function soporteCat() {
  var opt = $('#selectTipoCateSopor').get(0).value;
  console.log(opt);

  if (opt == "red") {
    document.getElementById("soporteEquipo").style.display = '';
 

  } else {
    document.getElementById("soporteEquipo").style.display = '';
    document.getElementById("inputDisp").style.display = '';
   
  }

}

function dispositivo() {

  var opt = document.getElementById('inputDispositivos').value;

  if (opt == "otro") {
    document.getElementById("inputOtro").style.display = '';
  } else {
    document.getElementById("inputOtro").style.display = 'none';
  }
}




