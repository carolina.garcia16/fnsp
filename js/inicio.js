$(document).ready(function () {
    var table = $('#tableInicio').DataTable();

    $('#casoinput').on('keyup', function () {
        table
            .columns(0)
            .search(this.value)
            .draw();
    });

    $('#descripinput').on('keyup', function () {
        table
            .columns(1)
            .search(this.value)
            .draw();
    });
    $('#dependenciainput').on('keyup', function () {
        table
            .columns(2)
            .search(this.value)
            .draw();
    });

    $('#solicitanteinput').on('keyup', function () {
        table
            .columns(3)
            .search(this.value)
            .draw();
    });
    $('#estadoinput').on('keyup', function () {
        table
            .columns(4)
            .search(this.value)
            .draw();
    });
    $('#prioridadinput').on('keyup', function () {
        table
            .columns(5)
            .search(this.value)
            .draw();
    });


    $('#tabSolicitudes a:first').on('click', function () {
        table
            .columns(4)
            .search("")
            .draw();
    });

    $('#tabSolicitudes a:eq( 1 )').on('click', function () {
        table
            .columns(4)
            .search("Abierto")
            .draw();
    });

    $('#tabSolicitudes a:last').on('click', function () {
        table
            .columns(4)
            .search("Cerrado")
            .draw();
    });

    function solicitud(caso, descripcion, dependencia, solicitante, estado, prioridad, asignado, fechaApertura, tiempoResolver, fechaActualizacion, fechaCierre) {
        this.caso = caso;
        this.descripcion = descripcion;
        this.dependencia = dependencia;
        this.solicitante = solicitante;
        this.estado = estado;
        this.prioridad = prioridad;
        this.asignado = asignado;
        this.fechaApertura = fechaApertura;
        this.tiempoResolver = tiempoResolver;
        this.fechaActualizacion = fechaActualizacion;
        this.fechaCierre = fechaCierre;
    }

    var solicitudes = [];
    var solicitud;


    for (var i = 1; i <= 3; i++) {
        if (i % 2 == 0) {
            solicitudNueva = new solicitud(i, "Impresora mala", "FNSP", "Juan", "abierta", "Alta", "Javier", "hoy", "3 días", "mañana", "pendiente");
            solicitudes.push(solicitudNueva);
        } else {
            solicitudNueva = new solicitud(i, "Impresora mala", "FNSP", "Juan", "abierta", "Media", "Javier", "hoy", "3 días", "mañana", "pendiente");
            solicitudes.push(solicitudNueva);
        }
    }

    solicitudNueva = new solicitud(9, "Impresora mala", "FNSP", "Juan", "abierta", "Baja", "Javier", "hoy", "3 días", "mañana", "pendiente");
    solicitudes.push(solicitudNueva);


    for (var i = 0; i < 9; i++) {
        var solicitud = solicitudes[i];
        if (solicitud.prioridad == "Alta") {
            table.row.add([solicitud.caso.toString(), solicitud.descripcion, solicitud.dependencia, solicitud.solicitante, solicitud.estado, solicitud.prioridad,
            solicitud.solicitante, solicitud.fechaApertura, solicitud.tiempoResolver, solicitud.fechaActualizacion, solicitud.fechaCierre]).draw();

        } else if (solicitud.prioridad == "Media") {
            table.row.add([solicitud.caso.toString(), solicitud.descripcion, solicitud.dependencia, solicitud.solicitante, solicitud.estado, solicitud.prioridad,
            solicitud.solicitante, solicitud.fechaApertura, solicitud.tiempoResolver, solicitud.fechaActualizacion, solicitud.fechaCierre]).draw();
        } else {
            table.row.add([solicitud.caso.toString(), solicitud.descripcion, solicitud.dependencia, solicitud.solicitante, solicitud.estado, solicitud.prioridad,
            solicitud.solicitante, solicitud.fechaApertura, solicitud.tiempoResolver, solicitud.fechaActualizacion, solicitud.fechaCierre]).draw();
        }
    }


});




