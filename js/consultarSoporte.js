$(document).ready(function() {
  var table = $('#consultarSolicitud').DataTable();

  $('#Codigoinput').on('keyup', function() {
    table
      .columns(0)
      .search(this.value)
      .draw();
  });

  $('#descripcioninput').on('keyup', function() {
    table
      .columns(1)
      .search(this.value)
      .draw();
  });

  $('#estadoCinput').on('keyup', function() {
    table
      .columns(3)
      .search(this.value)
      .draw();
  });

  $('#prioridadCinput').on('keyup', function() {
    table
      .columns(3)
      .search(this.value)
      .draw();
  });

  $('#Solicitanteinput').on('keyup', function() {
    table
      .columns(4)
      .search(this.value)
      .draw();
  });



  $("#consultarSolicitud").on('click', '.btnedit', function(event) {
    event.preventDefault();
    var numsol = $(this).closest('tr').find('td:first-child').text();
    var descripcion = $(this).closest('tr').find('td:nth-child(2)').text();
    var prioridad = $(this).closest('tr').find('td:nth-child(6)').text();
    var solicitante = $(this).closest('tr').find('td:nth-child(3)').text();
    var fechaInicio = $(this).closest('tr').find('td:nth-child(4)').text();

    $('.modal-title').html("Solicitud " + numsol);
    $('#inputPrioridad').val(prioridad);
    $('#inputSolici').val(solicitante);
    $('#inputFecha').val(fechaInicio);
    $('#inputDes').val(descripcion);

  });

  $("#consultarSolicitud").on("click", ".btnborrarS", function() {
    table.row($(this).parents('tr')).remove().draw(false);
  });

  $('#btnaceptS').click(function() {
    var row = table.row(this).index();
    var estado = $('#inputEstado option:selected').val();
    var asignado = $('#inputAsignarS option:selected').val();
    var prioridad = $('#inputChPriori option:selected').val();
    table.cell(row, 4).data(estado);
    table.cell(row, 5).data(prioridad);
    table.cell(row, 6).data(asignado);
  //  $('#asignarSolicitud').modal('hide');

  });

  $('#tabAsignar a:first').on('click', function() {
    table
        .columns( 4 )
        .search("")
        .draw();
  });

  $('#tabAsignar a:eq( 1 )').on('click', function() {
    table
        .columns( 4 )
        .search("Abierta")
        .draw();
  });

  $('#tabAsignar a:last').on('click', function() {
    table
        .columns( 4 )
        .search( "Gestionada")
        .draw();
  });




});


    $(document).ready(function() {
      var table = $('#tableAsignacion').DataTable();

        $('#btnaceptS ').click(function(){

            var nivel=$('#inputNivel option:selected').html();
            var f= new Date();
            var fecha=f.getDate()+"/"+(f.getMonth()+1)+"/"+f.getFullYear()+" "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();
            var prioridad=$('#inputChPriori option:selected').html();
            var asignado=$('#inputAsignarS option:selected').html();;


            document.getElementById("tableAsignacion").style.display='';
            table.row.add(["301",nivel,prioridad,asignado,fecha,"<a data-toggle='modal' href='#asignarSolicitud' style='margin-right: 5px; background:rgb(40, 167, 69);' class='btn btn-succes btn-sm btnedit'><i class='fas fa-edit' style='color:white;'></i></a><button style='margin-right: 5px;' type='button' class='btn btn-danger btn-sm btnborrarA'><i class='fa fa-times'></i></button>"
              ]).draw();

              // $('#asignarSolicitud').modal('hide');

        });

        $("#tableAsignacion").on("click", ".btnborrarA", function() {
          table.row($(this).parents('tr')).remove().draw(false);
        });

        $("#tableAsignacions").on('click', '.btnedit', function(event) {
          event.preventDefault();
          var numsol = $(this).closest('tr').find('td:first-child').text();
          var descripcion = $(this).closest('tr').find('td:nth-child(2)').text();
          var prioridad = $(this).closest('tr').find('td:nth-child(4)').text();
          var solicitante = $(this).closest('tr').find('td:nth-child(6)').text();
          var fechaInicio = $(this).closest('tr').find('td:nth-child(7)').text();

          $('.modal-title').html("Solicitud " + numsol);
          $('#inputPrioridad').val(prioridad);
          $('#inputSolici').val(solicitante);
          $('#inputFecha').val(fechaInicio);
          $('#inputDes').val(descripcion);

        });




  } );
