$(document).ready(function () {
  var table = $('#tableAsignacion').DataTable();
  $('#btnacept').click(function () {

    var nivel = $('#inputNivel option:selected').html();
    var fecha = $('#inputFecha2').val();
    var prioridad = $('#inputChPriori option:selected').html();
    var asignado = $('#inputAsignarS option:selected').html();
    var tipo = $('#inputTipo option:selected').html();
    var estado = $('#inputEstado option:selected').html();
    var obSol = $('#inputOb').val();

    document.getElementById("tableAsignacion").style.display = '';


    table.row.add(["301", nivel, prioridad, asignado, fecha, tipo, estado, obSol, "<a  data-toggle='modal' href='#editarAsignacion' style='margin-right: 5px; background:rgb(40, 167, 69);' class='btneditar btnedit'><i class='fa fa-list' style='color:white;'></i></a><button style='margin-right: 5px;' type='button' class='btnelimin btnborrarA'><i class='fa fa-times'></i></button>"]).draw();
    $('#asignarSolicitud').modal('hide');

  });

  $("#tableAsignacion").on("click", ".btnborrarA", function () {
    table.row($(this).parents('tr')).remove().draw(false);
  });

  $("#tableAsignacion").on('click', '.btnedit', function (event) {
    event.preventDefault();

    var tipoSol = $(this).closest('tr').find('td:nth-child(6)').text();
    var asignaSol = $(this).closest('tr').find('td:nth-child(4)').text();
    var nivelSol = $(this).closest('tr').find('td:nth-child(2)').text();
    var estadoSol = $(this).closest('tr').find('td:nth-child(7)').text();
    var prioriSol = $(this).closest('tr').find('td:nth-child(3)').text();
    var obSol = $(this).closest('tr').find('td:nth-child(8)').text();
    var fechaE = new Date();
    var editFecha = fechaE.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();

    $('#editFecha2').val(editFecha);
    $('#editTipo').val(tipoSol);
    $('#editAsignarS').val(asignaSol);
    $('#editNivel').val(nivelSol);
    $('#editEstado').val(estadoSol);
    $('#editChPriori').val(prioriSol);
    $('#editOb').val(obSol);

  });

  $('#btnEditarA').click(function () {
    var row = table.row(this).index();
    var nivel = $('#editNivel option:selected').html();
    var estado = $('#editEstado option:selected').html();
    var asignado = $('#editAsignarS option:selected').html();
    var prioridad = $('#editChPriori option:selected').html();
    var observacion = $('#editOb').val();
    var tipoAsignacion = $('#editTipo option:selected').html();
    var fechaActual = $('#editFecha2').val();

    table.cell(row, 1).data(nivel);
    table.cell(row, 2).data(prioridad);
    table.cell(row, 3).data(asignado);
    table.cell(row, 4).data(fechaActual);
    table.cell(row, 5).data(tipoAsignacion);
    table.cell(row, 6).data(estado);
    table.cell(row, 7).data(observacion);

    $('#editarAsignacion').modal('hide');

  });

});
