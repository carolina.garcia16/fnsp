$(document).ready(function() {
  var table = $('#consultarSolicitud').DataTable();

// #column3_search is a <input type="text"> element
$('#descripcioninput').on( 'keyup', function () {
    table
        .columns( 1 )
        .search( this.value )
        .draw();
} );

$('#estadoCinput').on( 'keyup', function () {
    table
        .columns( 3 )
        .search( this.value )
        .draw();
} );
$('#prioridadCinput').on( 'keyup', function () {
    table
        .columns( 4 )
        .search( this.value )
        .draw();
} );



$("#consultarSolicitud").on('click','.btnedit',function(event){
  event.preventDefault();
 var numsol = $(this).closest('tr').find('td:first-child').text();
 var descripcion=$(this).closest('tr').find('td:nth-child(2)').text();
 var prioridad=$(this).closest('tr').find('td:nth-child(4)').text();
 var solicitante=$(this).closest('tr').find('td:nth-child(6)').text();
 var fechaInicio=$(this).closest('tr').find('td:nth-child(7)').text();

 $('.modal-title').html("Solicitud "+numsol);
 $('#inputPrioridad').val(prioridad);
 $('#inputSolici').val(solicitante);
 $('#inputFecha').val(fechaInicio);
 $('#inputDes').val(descripcion);

});

$("#consultarSolicitud").on("click",".btnborrar",function(){
    table.row($(this).parents('tr')).remove().draw(false);
});

$("#consultarSolicitud").on("click",".btnborrar",function(){
    table.row($(this).parents('tr')).remove().draw(false);
});


} );
