function fechaSolicitud() {
  var f = new Date();
  var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();
  setTimeout('fechaSolicitud()', 1000);
  $('#inputFechaE').get(0).innerHTML = fecha;
}

$(document).ready(function () {
  var table = $('#tableSistemasInfo').DataTable();

  $('#btnSuccess').click(function () {

    var categoria = $('#selectTipoCateInfo option:selected').html();
    var f = new Date();
    var fecha = f.getDate() + "/" + (f.getMonth() + 1) + "/" + f.getFullYear() + " " + f.getHours() + ":" + f.getMinutes() + ":" + f.getSeconds();
  
    table.row.add(["1", categoria, "Asignado", fecha, "<button style='' type='button' class='btn btn-danger btn-sm btnborrarA'><i class='fa fa-times'></i></button>"
    ]).draw();

  });

  $("#tableSistemasInfo").on("click", ".btnborrarA", function () {
    table.row($(this).parents('tr')).remove().draw(false);
  });

});

function categorias() {

  var opt = document.getElementById('selectTipoCateInfo').value;

  document.getElementById("listadoDependencias").style.display = 'none';
  document.getElementById("categoriaAsesorias").style.display = 'none';
  document.getElementById("categoriaCorrecion").style.display = 'none';
  document.getElementById("categoriaListados").style.display = 'none';
  document.getElementById("categoriaGestion").style.display = 'none';
  document.getElementById("categoriaNuevasFuncionalidades").style.display = 'none';
  document.getElementById("categoriaErrores").style.display = 'none';
  document.getElementById("categoriaCapacitacion").style.display = 'none';
  document.getElementById("btnSuccess").style.display='none';
  document.getElementById("listadoProyectos").style.display = 'none'; 
  document.getElementById('listadoSistemasAsesoria').style.display='none';
  document.getElementById("descripcionObjeto").style.display = 'none';
    
  var sistema= '<label for="inputSistemas" style="font-weight:bold">Sistemas de información</label><i class="fa fa-question-circle" aria-hidden="true" title="Debe seleccionar el sistema de información"></i><select id="selectSistema" class="form-control"><option value="0">Seleccione</option><option value="1">BUHO</option><option value="2">EVA</option><option value="3">Evaluación docente</option><option value="4">SIAC</option> <option value="5">SIAU</option><option value="6">SIGEP</option> <option value="7">SISO</option><option value="8">SISREC</option><option value="9">SOCA</option><option value="10">SSOFI</option></select>'
  
  switch (opt) {

    case '1':
      var select='<label for="" style="font-weight:bold">Tipo de asesoría</label><i class="fa fa-question-circle" aria-hidden="true" title="Debe seleccionar el tipo de asesoria que requiere"></i><select id="selectAsesorias" class="form-control"><option>Seleccione</option><option value="">Actualización</option><option value="">Desarrollo </option><option value="">Diseño</option><option value="">Planificación</option><option value="">Puesta en producción</option></select>'
      document.getElementById('listadoSistemasAsesoria').style.display='';
      document.getElementById('listadoSistemasAsesoria').innerHTML = select;
      document.getElementById("listadoDependencias").style.display = '';
      document.getElementById("categoriaAsesorias").style.display = '';
      document.getElementById("descripcionObjeto").style.display = '';
      document.getElementById("btnSuccess").style.display='';
      
      break;

    case '2':
      document.getElementById('listadoSistemasAsesoria').style.display='';
      document.getElementById('listadoSistemasAsesoria').innerHTML = sistema;
      document.getElementById("listadoDependencias").style.display = '';
      document.getElementById("categoriaCorrecion").style.display = '';
      document.getElementById("btnSuccess").style.display='';

      break;

    case '3':
      document.getElementById('listadoSistemasAsesoria').style.display='';
      document.getElementById('listadoSistemasAsesoria').innerHTML = sistema;
      document.getElementById("listadoDependencias").style.display = '';
      document.getElementById("categoriaListados").style.display = '';
      document.getElementById("btnSuccess").style.display='';
      break;

    case '4':
      document.getElementById('listadoSistemasAsesoria').style.display='';
      document.getElementById('listadoSistemasAsesoria').innerHTML = sistema;
      document.getElementById("listadoDependencias").style.display = '';
      document.getElementById("categoriaGestion").style.display = '';
      document.getElementById("btnSuccess").style.display='';
      break;

    case '5':
      document.getElementById('listadoSistemasAsesoria').style.display='';
      document.getElementById('listadoSistemasAsesoria').innerHTML = sistema;
      document.getElementById("listadoDependencias").style.display = '';
      document.getElementById("categoriaNuevasFuncionalidades").style.display = '';
      document.getElementById("btnSuccess").style.display='';
      break;

    case '6':
      document.getElementById('listadoSistemasAsesoria').style.display='';
      document.getElementById('listadoSistemasAsesoria').innerHTML = sistema;
      document.getElementById("listadoDependencias").style.display = '';
      document.getElementById("categoriaErrores").style.display = '';
      document.getElementById("btnSuccess").style.display='';
      break;

    case '7':
      document.getElementById('listadoSistemasAsesoria').style.display='';
      document.getElementById('listadoSistemasAsesoria').innerHTML = sistema;
      document.getElementById("listadoDependencias").style.display = '';
      document.getElementById("categoriaCapacitacion").style.display = '';
      document.getElementById("btnSuccess").style.display='';
      break;
  }
 
}

function option(){
  var proyecto =document.getElementsByName('option');
  if(proyecto[0].checked){
    document.getElementById("listadoProyectos").style.display = ''; 
  }else if(proyecto[1].checked){
    document.getElementById("listadoProyectos").style.display = 'none'; 
  }
}



